package com.epam.task_Degue;

public class TestDeque {
    public static void main(String[] args) {
        runDeque();
    }

    private static void runDeque(){
        Deque<String> deque = new Deque<>();
        deque.offer("11");
        deque.offer("22");
        deque.offer("33");
        deque.offer("44");
        deque.offer("55");
        deque.offer("66");
        deque.peek();
        deque.peekFirst();
        deque.peekLast();
        deque.poll();
        deque.pollFirst();
        deque.pollLast();
        deque.size();
        deque.push("1256");
        deque.peekFirst();
    }
}
