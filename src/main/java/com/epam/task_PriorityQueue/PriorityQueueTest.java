package com.epam.task_PriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Comparator;

public class PriorityQueueTest<E> /*extends AbstractQueue*/ {
    static final Logger LOGGER = LogManager.getLogger(PriorityQueueTest.class.getName());
    private static final int INITIAL_CAPACITY = 10;
    private E[] queue;
    private int size = 0;
    private final Comparator<? super E> comparator;

    public PriorityQueueTest() {
        queue = (E[]) new Object[INITIAL_CAPACITY];
        comparator = null;
    }

    public PriorityQueueTest(Comparator<? super E> comparator) {
        this.comparator = comparator;
    }

    private void grow() {
        int oldCapacity = queue.length;
        int newCapacity = (int) (oldCapacity * 1.5) + 1;
        queue = Arrays.copyOf(queue, newCapacity);
    }

    public boolean add(E e) {
        if (offer(e))
            return true;
        else
            throw new IllegalStateException("PriorityQueue full");
    }

    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        int i = size;
        if (i >= queue.length) {
            grow();
        }
        size++;
        if (i == 0) {
            queue[0] = e;
        } else {
            siftUp(i, e);
        }
        return true;
    }

    private void siftUp(int i, E x) {
        if (comparator != null) {
            siftUpUsingComparator(i, x);
        } else {
            siftUpUseComparable(i, x);
        }
    }

    private void siftUpUsingComparator(int k, E x) {
        while (k > 0) {
            int previous = k - 1;
            E e = queue[previous];
            if (comparator.compare(x, e) >= 0) {
                break;
            }
            queue[k] = e;
            k = previous;
        }
        queue[k] = x;
    }

    private void siftUpUseComparable(int k, E x) {
        Comparable<? super E> key = (Comparable<? super E>) x;
        while (k > 0) {
            int previous = k - 1;
            E e = queue[previous];
            if (key.compareTo(e) >= 0) {
                break;
            }
            queue[k] = e;
            k = previous;
        }
        queue[k] = (E) key;
    }

    private int indexOf(E o) {
        if (o != null) {
            for (int i = 0; i < size; i++) {
                if (o.equals(queue[i]))
                    return i;
            }
        }
        return -1;
    }

    public int size() {
        return size;
    }

    public boolean remove() {
        E deleted = poll();
        if (deleted != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(E o) {
        int i = indexOf(o);
        if (i == -1) {
            return false;
        } else {
            removeAt(i);
            return true;
        }
    }

    private E removeAt(int i) {
        int s = --size;
        if (s == 1) {
            queue[i] = null;
        } else {
            E moved = queue[s];
            queue[s] = null;
            siftDown(i, moved);
            if (queue[i] == moved){
                siftUp(i, moved);
                if(queue[i] != moved){
                    return moved;
                }
            }
        }
        return null;
    }

    public E peek() {
        return (size == 0) ? null : queue[0];
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            queue[i] = null;
        }
        size = 0;
    }

    public E poll() {
        if (size == 0) {
            return null;
        }
        int s = --size;
        E result = queue[0];
        E x = queue[s];
        queue[s] = null;
        if (s != 0) {
            siftDown(0, x);
        }
        return result;
    }

    private void siftDown(int k, E x) {
        if (comparator != null) {
            siftDownUsingComparator(k, x);
        } else {
            siftDownUseComparable(k, x);
        }
    }

    private void siftDownUsingComparator(int k, E x) {
        queue[k] = queue[k + 1];
        while (k < (size - 1)) {
            int next = k + 1;
            E c = queue[next];
            int right = next + 1;
            if (right < size && comparator.compare(c, queue[right]) > 0) {
                c = queue[next = right];
            }
            if (comparator.compare(x, c) <= 0) {
                break;
            }
            queue[k] = c;
            k = next;
        }
        queue[k] = x;
    }

    private void siftDownUseComparable(int k, E x) {
        Comparable<? super E> key = (Comparable<? super E>) x;
        queue[k] = queue[k + 1];
        while (k < (size - 1)) {
            int next = k + 1;
            E c = queue[next];
            int right = next + 1;
            if (right < size && ((Comparable<? super E>) c).compareTo(queue[right]) > 0) {
                c = queue[next = right];
            }
            if (key.compareTo(c) <= 0) {
                break;
            }
            queue[k] = c;
            k = next;
        }
        queue[k] = (E) key;
    }

    public void print(){
        for (int i = 0; i < size; i++) {
            LOGGER.info(queue[i]);
        }
    }

    @Override
    public String toString() {
        return "PriorityQueueTest{" +
                "queue=" + Arrays.toString(queue) +
                ", size=" + size +
                ", comparator=" + comparator +
                '}';
    }
}