package com.epam.task_PriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestPriorityQueue {
    static final Logger logger = LogManager.getLogger(TestPriorityQueue.class.getName());

    public static void main(String[] args) {
        createPriorityQueue();
    }

    private static void createPriorityQueue() {
        PriorityQueueTest<Integer> integers = new PriorityQueueTest<>();
        integers.offer(1);
        integers.offer(11);
        integers.offer(3);
        integers.offer(5);
        integers.offer(7);
        integers.offer(9);
        printQueue(integers);
    }

    private static void printQueue(PriorityQueueTest<Integer> integers) {
        System.out.println("Start");
        System.out.println(integers.toString());
        System.out.println("End");
    }
}

class Person {
}
