package com.epam.Task_BinaryTree;

public class BinaryTree<K extends Comparable<K>, V> {
    private Node<K, V> root;

    public BinaryTree() {
        root = null;
    }

    public boolean add(K key, V value) {
        if (key == null) {
            return false;
        }
        if (value == null) {
            return false;
        }
        Node<K, V> newNode = new Node<>(key, value);
        Node<K, V> current = root;
        if (current == null) {
            root = newNode;
            return true;
        }
        while (true) {
            Node<K, V> parent = current;
            if (current.key.compareTo(key) > 0) {
                current = current.leftChild;
                if (current == null) {
                    parent.leftChild = newNode;
                    break;
                }
            } else {
                current = current.rightChild;
                if (current == null) {
                    parent.rightChild = newNode;
                    break;
                }
            }
        }
        return true;
    }

    public boolean remove(K key) {
        if (key == null) {
            return false;
        }
        if (root == null) {
            return false;
        }
        Node<K, V> current = root;
        Node<K, V> parent = root;
        boolean isLeftChild = true;
        while (current.key.compareTo(key) != 0) {
            parent = current;
            if (current.key.compareTo(key) > 0) {
                isLeftChild = true;
                current = current.leftChild;
            } else {
                isLeftChild = false;
                current = current.rightChild;
            }
            if (current == null) {
                return false;
            }
        }
        Node<K, V> deleteNode = current;
        if (current.leftChild == null && current.rightChild == null) {
            deleteNodeHasNoChildren(parent, current, isLeftChild);
        } else if (current.rightChild == null || current.leftChild == null) {
            deleteNodeHasOneChild(parent, current, isLeftChild);
        } else {
            deleteNodeHasChildren(parent, current, isLeftChild);
        }
        return true;
    }

    public Node<K, V> find(K key) {
        if (key == null) {
            return null;
        }
        if (root == null) {
            return null;
        }
        Node<K, V> current = root;
        while (current.key.compareTo(key) != 0) {
            if (current.key.compareTo(key) > 0) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
            if (current == null) {
                return null;
            }
        }
        return current;
    }

    public Node<K, V> min() {
        if (root == null) {
            return null;
        }
        Node<K, V> current = root;
        Node<K, V> previous;
        do {
            previous = current;
            current = current.leftChild;
        } while (current != null);
        return previous;
    }

    public Node<K, V> max() {
        if (root == null) {
            return null;
        }
        Node<K, V> current = root;
        Node<K, V> previous;
        do {
            previous = current;
            current = current.rightChild;
        } while (current != null);
        return previous;
    }

    private void deleteNodeHasChildren(Node<K, V> parent, Node<K, V> current, boolean isLeftChild) {
        Node<K, V> inheritor = getInheritor(current);
        if (current == root) {
            root = inheritor;
        } else if (isLeftChild) {
            parent.leftChild = inheritor;
        } else {
            parent.rightChild = inheritor;
        }
        inheritor.leftChild = current.leftChild;
    }

    private Node<K, V> getInheritor(Node<K, V> deleteNode) {
        Node<K, V> inheritorParent = deleteNode;
        Node<K, V> inheritor = deleteNode;
        Node<K, V> current = deleteNode.rightChild;
        while (current != null) {
            inheritorParent = inheritor;
            inheritor = current;
            current = current.leftChild;
        }
        if (inheritor != deleteNode.rightChild) {
            inheritorParent.leftChild = inheritor.rightChild;
            inheritor.rightChild = deleteNode.rightChild;
        }
        return inheritor;
    }

    private void deleteNodeHasOneChild(Node<K, V> parent, Node<K, V> current, boolean isLeftChild) {
        if (current.rightChild == null) {
            if (current == root) {
                root = current.leftChild;
            } else if (isLeftChild) {
                parent.leftChild = current.leftChild;
            } else {
                parent.rightChild = current.leftChild;
            }
        } else {
            if (current == root) {
                root = current.rightChild;
            } else if (isLeftChild) {
                parent.leftChild = current.rightChild;
            } else {
                parent.rightChild = current.rightChild;
            }
        }
    }

    private void deleteNodeHasNoChildren(Node<K, V> parent, Node<K, V> current, boolean isLeftChild) {
        if (current == root) {
            root = null;
        } else if (isLeftChild) {
            parent.leftChild = null;
        } else {
            parent.rightChild = null;
        }
    }

    public static class Node<K, V> {
        private K key;
        private V value;
        private Node<K, V> rightChild;
        private Node<K, V> leftChild;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
            rightChild = null;
            leftChild = null;
        }

        @Override
        public String toString() {
            return "Node[" +
                    "key=" + key +
                    ", value=" + value +
                    ']';
        }
    }
}